import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import gin
from torch.nn.modules.loss import _Loss

# Defining the repulsive losses
@gin.configurable
class CustomCrossEntropyLossContinuous(nn.CrossEntropyLoss):
    def __init__(self, bandwidth, *args):
        self.bandwidth = bandwidth
        super().__init__(*args)

    def forward(self, input_logits, target_logits):
        assert len(input_logits.size()) == 2, 'wrong size for input logits'
        assert len(target_logits.size()) == 2, 'wrong size for target logits'

        a = F.softmax(target_logits, 1)
        a_log = F.log_softmax(target_logits, 1)
        b = F.log_softmax(input_logits, 1)

        xent = - (a * b).sum(1)
        ent = - (a * a_log).sum(1)  # Entropy of targets
        rbf_xent = torch.exp(-((xent - ent) ** 2) / (2 * self.bandwidth ** 2))

        return torch.mean(rbf_xent)

@gin.configurable
class RegressionRepulsiveLoss(nn.MSELoss):
    def __init__(self, bandwidth, *args):
        self.bandwidth = bandwidth
        super().__init__(*args)

    def forward(self, net_preds, reference_preds):
        return torch.mean(torch.exp(-(1/(2*self.bandwidth**2))*torch.norm(net_preds - reference_preds, dim=1,keepdim=True)**2))

@gin.configurable
class JensenShannonDistance(_Loss):
    def __init__(self, *args):
        super().__init__(*args)
    def forward(self, a, b):
        '''
        a: logit
        b: logit
        '''
        kl = torch.nn.KLDivLoss(reduction='none')
        ap = torch.log_softmax(a, 1)
        bp = torch.log_softmax(b, 1)
        m = .5 * (ap.exp() + bp.exp())
        # We had a max (0, .) to avoid instabilities leading the distance to be
        # negative
        dist = torch.max(torch.zeros(1), 0.5 * kl(torch.log(m),F.softmax(a, 1)).sum(1) + 0.5 * kl(torch.log(m),F.softmax(b, 1)).sum(1))
        return -torch.log(math.exp(-10)*torch.ones_like(dist) + dist).mean()

class HLoss(nn.Module):
    r'''
    Entropy of a __probability vector__
    '''
    def __init__(self):
        super().__init__()
        self.eps = 1e-20

    def forward(self, x):
        b = x * torch.log(x + self.eps)
        b = -1.0 * b.sum()
        return b
