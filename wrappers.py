import torch
import numpy as np
import gin
import gym

@gin.configurable(blacklist=['env'])
class CodingWrapper(gym.Wrapper):
    r'''
    Wrapper around a gym environment that discretize the position and velocity of the MountainCar env into 32 bins each
    '''
    def __init__(self, env, coding_fn):
        r'''
        :param env: gym env
        :param coding_fn: Function encoding an observation, fron a ndarray (size_obs,) to an encoded ndarray (size_features,)
        '''
        super().__init__(env)
        self.env = env
        self.encode = coding_fn

    def reset(self, **kwargs):
        x = self.env.reset(**kwargs)
        x = self.encode(x)
        return torch.from_numpy(x).float().view(1, -1)

    def step(self, action):
        res = self.env.step(action) # First el of list is observation
        x = torch.from_numpy(self.encode(res[0])).float().view(1, -1)
        return (x,) + res[1:]

# Identity (no coding)
def identity_encoding(obs):
    return obs

# Independent coding

@gin.configurable(blacklist=['x'])
def independent_encoding(x, b=32):
    r'''
    coding_fn, discretizes _x_.
    :param b: (int) number of bins
    :returns: (torch.tensor, torch.tensor)
    '''
    # Normalize
    pos, vel = normalize(x)
    bins = np.linspace(0, 1, b+1)
    idx_pos = np.digitize(pos, bins) - 1
    idx_vel = np.digitize(vel, bins) - 1
    zpos = np.zeros(len(bins) - 1)
    zvel = np.zeros(len(bins) - 1)
    zpos[idx_pos], zvel[idx_vel] = 1, 1
    return np.concatenate([zpos, zvel], 0)

@gin.configurable
def get_encoder(name):
    if name == 'independent':
        return independent_encoding
    elif name == 'identity':
        return identity_encoding
    elif name == 'tile':
        return tile_encoding
    else:
        return None

# Tile coding
def tile_get_intervals(n, b):
    r'''
    Creates the tiles for a 2d observation space where each dimension is of dimension 1.
    env: (environment) Gym 2d environment
    n: (int) number of tiles
    b: (int) number of bins per dimension per tile
    '''
    # We process first the position and then the velocity
    len_per_dim = 1
    w = len_per_dim / (b - 1 + 1/n)
    u = w / n
    base_interval = np.asarray([i*w for i in range(b)])
    l_intervals = [(base_interval, base_interval)]

    # We now create the intervals for the remaining tilings
    for i in range(1, n):
        l_intervals.append((base_interval - i * u, base_interval - i * u))

    return l_intervals

@gin.configurable(blacklist=['obs'])
def tile_encoding(obs, n=1, b=32):
    r'''
    Encodes an observation (output from the environment) into a tile code.
    obs: (np.ndarray) unnormalized obs
    '''

    # Normalization
    tiles_intervals = tile_get_intervals(n, b)
    pos_, vel_ = normalize(obs)
    # Encoding
    feat = []
    for inter_pos, inter_vel in tiles_intervals:
        idx_pos = np.digitize(pos_, inter_pos) - 1 # Get the bin
        idx_vel = np.digitize(vel_, inter_vel) - 1
        idx = np.ravel_multi_index((idx_pos, idx_vel), (len(inter_pos), len(inter_vel))).squeeze().reshape(-1, 1)
        z = np.zeros((len(inter_pos)**2,)).astype(int)
        z[idx] = 1
        feat.append(z)

    return np.concatenate(feat, 0)

def normalize(obs):
    pos, vel = np.split(obs, 2)
    pos_ = (pos + 1.2) / 1.8
    vel_ = (vel + .07) / .14
    return pos_, vel_

def phi_encode(obs, a):
    dim_obs = obs.shape[1]
    z = torch.zeros((1, dim_obs*3))
    z[0, dim_obs*a:dim_obs*(a+1)] = obs
    return z



'''
# Code to test the independent encoding
import gym
env = gym.make('MountainCar-v0')
n_bins = 32
x = env.reset()
import torch
res = discretize(x, np.linspace(env.observation_space.low[0], env.observation_space.high[0], n_bins+1), np.linspace(env.observation_space.low[1], env.observation_space.high[1], n_bins+1))
print(res)
'''

'''
# Code to test the tile coding
import gym
env = gym.make('MountainCar-v0')
x = env.reset()

import torch
ans = tile_get_intervals(5, 32)
res = tile_encoding(x, ans)
print(res)

'''
