import model
import torch
import torch.optim as optim
import torch.nn as nn
from torch.distributions import Multinomial
import numpy as np
import random
import layers
import gin
import copy
import utils
import layers

@gin.configurable
class A3CLearner(object):
    r'''
    DQN from Osband's implementation
    '''

    name = 'a3c'
    def __init__(self, online_net, discount_factor, optimizer_name, criterion_name,
            period_async_update, entropy_regularization, beta, device=torch.device('cpu')):

        self.discount_factor = discount_factor
        # Preparing the loss function
        self.criterion = utils.criterion_builder(criterion_name)()
        self.entropy_criterion = layers.HLoss()
        self.period_async_update = period_async_update
        self.optimizer = utils.optimizer_builder(optimizer_name)(online_net.parameters())

        # Entropy regularization
        self.entropy_regularization = entropy_regularization
        self.beta = beta

    def compute_loss(self, thread_net, l_transitions):
        r'''
        Returns the loss associated to a transition.
        Does __NOT__ perform an optimization step!
        '''
        last_transition = l_transitions[-1]
        if last_transition.done:
            R = torch.zeros(1)
        else:
            R = thread_net.forward_critic(last_transition.next_obs) # TODO : obs or next obs?

        value_loss = torch.zeros(1)
        policy_loss = torch.zeros(1)
        for transition in l_transitions[::-1]:
            R = transition.reward + self.discount_factor * R
            # First compute the term wrt the value function, without using gradients
            A = (R - thread_net.forward_critic(transition.obs).detach()).view(-1)
            actions_logprobas = thread_net.logforward_actor(transition.obs)
            policy_loss.add_(A * actions_logprobas[0, transition.action].view(-1))

            # Add the entropy term
            if self.entropy_regularization:
                entropy = self.entropy_criterion(actions_logprobas.exp().view(1, -1))
                policy_loss.add_(self.beta * entropy)

            # Then, value loss
            value_loss.add_(self.criterion(thread_net.forward_critic(transition.obs), R))

        return value_loss - policy_loss

    def update(self, loss, thread_net, online_net):
        self.optimizer.zero_grad()
        # Compute the gradients __ ON THE THREAD NET __
        loss.backward()
        # Copy the grads on the master network
        for thread_parameter, online_parameter in zip(thread_net.parameters(), online_net.parameters()):
            online_parameter._grad = thread_parameter.grad # _grad is writable while grad is not (https://discuss.pytorch.org/t/difference-between-grad-and--grad/4816/7)

        # Optimization is done on the parameters of the online net
        self.optimizer.step()

    def act(self, thread_net, obs):
        return self.sample_action(thread_net, obs)

    def act_greedy(self, thread_net, obs):
        """
        :param state:
        :return:
        """
        with torch.no_grad():
            res = thread_net.forward_actor(obs)
            return res.max(1)[1].view(-1, 1), res.view(-1, 1)

    def sample_action(self, thread_net, obs):
        """
        Only works for one state (no batches !)
        :param state:
        :return:
        """
        pred_probs = thread_net.forward_actor(obs)
        d = Multinomial(1, probs=pred_probs)
        action = d.sample().max(1)[1] # Get the index of the action
        return action.view(-1, 1)
