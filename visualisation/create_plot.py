#!/usr/bin/env python
# coding: utf-8
import matplotlib
matplotlib.use('agg')
import torch
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(font_scale=1.9, rc={'text.usetex' : True})
import os
from pathlib import Path
import pickle
# Interpolate to be able to average
from scipy.interpolate import interp1d
savefigs_path = Path('figs')

x = np.linspace(10000, 990000, 9901)
def interpolate_data(x, l_t, l_g):
    l = []
    for t, g in zip(l_t, l_g):
        f = interp1d(t, g)
        l.append(f(x))
    return l

def prepare_plots(paths, idx, idy):
    basepath = Path('results')
    means, stds = [], []
    for p in paths:
        l = []
        for i in range(5):
            l.append(np.load(basepath / p / 'results_seed:{}_id:{}.npy'.format(i, i)))

        # show returns
        abs_ = [e[:, idx] for e in l]
        ord_ = [e[:, idy] for e in l]

        res = np.stack(interpolate_data(x, abs_, ord_), 1)
        m_, s_ = res.mean(1), res.std(1)

        m_smooth = np.convolve(m_, np.ones((200,))/200, mode='valid')
        s_smooth = np.convolve(s_, np.ones((200,))/200, mode='valid')

        means.append(m_smooth)
        stds.append(s_smooth)

    return means, stds

myxticks = [0, '2e5', '4e5','6e5', '8e5', '1e6']

# --------------------------------------------------------------------------------
# Q Learning plots
# --------------------------------------------------------------------------------

# Load files
paths = [Path('qlearning_single_indep_alpha:.05'), Path('qlearning_single_indep_alpha:.1'), Path('qlearning_single_indep_alpha:.25')]
means, stds = prepare_plots(paths, 0, 2) # 2: return

colors = ['b', 'r', 'y']
lrs = ['.05', '.1', '.25']
plt.figure(figsize=(7, 7))
for i, (m_smooth, s_smooth) in enumerate(zip(means, stds)):
    plt.plot(range(len(m_smooth)), m_smooth, c=colors[i])
    plt.fill_between(range(len(m_smooth)), m_smooth - s_smooth, m_smooth + s_smooth, color=colors[i], alpha=.2, label='lr = {}'.format(lrs[i]))
plt.legend()
plt.title('Q-Learning, comparison of the returns \n 3 learning rates, using 1 process')
locs, labels = plt.xticks()
plt.xticks(locs[1:-1], labels=myxticks)
plt.xlabel('\# of (global) timesteps')
plt.ylabel('Returns')
plt.legend()
figname = 'qlearning-returns-1.pdf'
plt.tight_layout()
plt.savefig(savefigs_path / figname)

# --------------------------------------------------------------------------------

# Load files
paths = [Path('qlearning_multi_indep_alpha:.05'), Path('qlearning_multi_indep_alpha:.1'), Path('qlearning_multi_indep_alpha:.25')]
means, stds = prepare_plots(paths, 0, 2)

colors = ['b', 'r', 'y']
lrs = ['.05', '.1', '.25']
plt.figure(figsize=(7, 7))
for i, (m_smooth, s_smooth) in enumerate(zip(means, stds)):
    plt.plot(range(len(m_smooth)), m_smooth, c=colors[i])
    plt.fill_between(range(len(m_smooth)), m_smooth - s_smooth, m_smooth + s_smooth, color=colors[i], alpha=.2, label='lr = {}'.format(lrs[i]))
plt.legend()
plt.title('Q-Learning, comparison of the returns \n 3 learning rates, using 4 processes')
locs, labels = plt.xticks()
plt.xticks(locs[1:-1], labels=myxticks)
plt.xlabel('\# of (global) timesteps')
plt.ylabel('Returns')
plt.legend()
figname = 'qlearning-returns-4.pdf'
plt.tight_layout()
plt.savefig(savefigs_path / figname)

# --------------------------------------------------------------------------------
#  DQN plots
# --------------------------------------------------------------------------------

# Load files
paths = [Path('dqn_single_indep_alpha:.001'), Path('dqn_single_indep_alpha:.01'), Path('dqn_single_indep_alpha:.1')]
means, stds = prepare_plots(paths, 0, 2)

colors = ['b', 'r', 'y']
lrs = ['.001', '.01', '.1']
plt.figure(figsize=(7, 7))
for i, (m_smooth, s_smooth) in enumerate(zip(means, stds)):
    plt.plot(range(len(m_smooth)), m_smooth, c=colors[i])
    plt.fill_between(range(len(m_smooth)), m_smooth - s_smooth, m_smooth + s_smooth, color=colors[i], alpha=.2, label='lr = {}'.format(lrs[i]))
plt.legend()
plt.title('DQN, comparison of the returns \n 3 learning rates, using 1 process')
locs, labels = plt.xticks()
plt.xticks(locs[1:-1], labels=myxticks)
plt.xlabel('\# of (global) timesteps')
plt.ylabel('Returns')
plt.legend()
figname = 'dqn-returns-1.pdf'
plt.tight_layout()
plt.savefig(savefigs_path / figname)

# Load files
paths = [Path('dqn_multi_indep_alpha:.001'), Path('dqn_multi_indep_alpha:.01'), Path('dqn_multi_indep_alpha:.1')]
means, stds = prepare_plots(paths, 0, 2)

colors = ['b', 'r', 'y']
lrs = ['.001', '.01', '.1']
plt.figure(figsize=(7, 7))
for i, (m_smooth, s_smooth) in enumerate(zip(means, stds)):
    plt.plot(range(len(m_smooth)), m_smooth, c=colors[i])
    plt.fill_between(range(len(m_smooth)), m_smooth - s_smooth, m_smooth + s_smooth, color=colors[i], alpha=.2, label='lr = {}'.format(lrs[i]))
plt.legend()
plt.title('DQN, comparison of the returns \n 3 learning rates, using 4 processes')
locs, labels = plt.xticks()
plt.xticks(locs[1:-1], labels=myxticks)
plt.xlabel('\# of (global) timesteps')
plt.ylabel('Returns')
plt.legend()
figname = 'dqn-returns-4.pdf'
plt.tight_layout()
plt.savefig(savefigs_path / figname)

# --------------------------------------------------------------------------------
#  A3C plots
# --------------------------------------------------------------------------------

# Load files
paths = [Path('a3c_single_indep_alpha:.001'), Path('a3c_single_indep_alpha:.01'), Path('a3c_single_indep_alpha:.1')]
means, stds = prepare_plots(paths, 0, 2)

colors = ['b', 'r', 'y']
lrs = ['.001', '.01', '.1']
plt.figure(figsize=(7, 7))
for i, (m_smooth, s_smooth) in enumerate(zip(means, stds)):
    plt.plot(range(len(m_smooth)), m_smooth, c=colors[i])
    plt.fill_between(range(len(m_smooth)), m_smooth - s_smooth, m_smooth + s_smooth, color=colors[i], alpha=.2, label='lr = {}'.format(lrs[i]))
plt.legend()
plt.title('A3C, comparison of the returns \n 3 learning rates, using 1 process')
locs, labels = plt.xticks()
plt.xticks(locs[1:-1], labels=myxticks)
plt.xlabel('\# of (global) timesteps')
plt.ylabel('Returns')
plt.legend()
figname = 'a3c-returns-1.pdf'
plt.tight_layout()
plt.savefig(savefigs_path / figname)

# Load files
paths = [Path('a3c_multi_indep_alpha:.001'), Path('a3c_multi_indep_alpha:.01'), Path('a3c_multi_indep_alpha:.1')]
means, stds = prepare_plots(paths, 0, 2)

colors = ['b', 'r', 'y']
lrs = ['.001', '.01', '.1']
plt.figure(figsize=(7, 7))
for i, (m_smooth, s_smooth) in enumerate(zip(means, stds)):
    plt.plot(range(len(m_smooth)), m_smooth, c=colors[i])
    plt.fill_between(range(len(m_smooth)), m_smooth - s_smooth, m_smooth + s_smooth, color=colors[i], alpha=.2, label='lr = {}'.format(lrs[i]))
plt.legend()
plt.title('A3C, comparison of the returns \n 3 learning rates, using 4 processes')
locs, labels = plt.xticks()
plt.xticks(locs[1:-1], labels=myxticks)
plt.xlabel('\# of (global) timesteps')
plt.ylabel('Returns')
plt.legend()
figname = 'a3c-returns-4.pdf'
plt.tight_layout()
plt.savefig(savefigs_path / figname)

# --------------------------------------------------------------------------------

# Load files
paths = [Path('a3c_multi_indep_alpha:.001'), Path('a3c_multi_indep_alpha:.01'), Path('a3c_multi_indep_alpha:.1')]
means, stds = prepare_plots(paths, 0, 3)

colors = ['b', 'r', 'y']
lrs = ['.001', '.01', '.1']
plt.figure(figsize=(7, 7))
for i, (m_smooth, s_smooth) in enumerate(zip(means, stds)):
    plt.plot(range(len(m_smooth)), m_smooth, c=colors[i])
    plt.fill_between(range(len(m_smooth)), m_smooth - s_smooth, m_smooth + s_smooth, color=colors[i], alpha=.2, label='lr = {}'.format(lrs[i]))
locs, labels = plt.xticks()
plt.xticks(locs[1:-1], labels=myxticks)
plt.xlabel('\# of (global) timesteps')
plt.ylabel('Policy entropy')
plt.legend()
plt.title('A3C, comparison of the entropy \n 3 learning rates, using 4 processes')
figname = 'a3c-entropy.pdf'
plt.tight_layout()
plt.savefig(savefigs_path / figname)

