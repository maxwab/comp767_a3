import model
import torch
import torch.optim as optim
import torch.nn as nn
import numpy as np
import random
import layers
import gin
import copy
import utils

@gin.configurable
class DQNLearner(object):
    r'''
    DQN from Osband's implementation
    '''

    name = 'dqn'
    def __init__(self, online_net,
            discount_factor, eps_start_value, possible_eps_end_values, eps_len_decay,
            optimizer_name, criterion_name, double_q,
            period_target_update, period_async_update, device=torch.device('cpu')):

        self.double_q = double_q
        self.discount_factor = discount_factor
        # Preparing the loss function
        self.criterion = utils.criterion_builder(criterion_name)()
        self.eps_start_value = eps_start_value
        self.eps_end_value = utils.choose_eps(possible_eps_end_values)
        self.eps_len_decay = eps_len_decay
        self.period_target_update = period_target_update
        self.period_async_update = period_async_update
        self.optimizer = utils.optimizer_builder(optimizer_name)(online_net.parameters())

        # Giving information on the type of learner
        print('Learner: eps_end_value={}'.format(self.eps_end_value))

    def compute_loss(self, online_net, target_net, transition):
        r'''
        Returns the loss associated to a transition.
        Does __NOT__ perform an optimization step!
        '''

        if not self.double_q:
            if transition.done: # This condition avoids a costly forward pass if the state is terminal
                qlearning_target = transition.reward
            else:
                next_max_state_action_values = target_net(transition.next_obs).detach().max(1)[0]
                qlearning_target = transition.reward + self.discount_factor * next_max_state_action_values
        else:
            pass

        state_action_values = online_net(transition.obs)[0, transition.action]  # Compute Q(s_t, a), only one element per batch

        # Actually compute the loss
        loss = self.criterion(state_action_values, qlearning_target)
        return loss

    def act(self, online_net, obs, steps_done):
        return self.act_eps_greedy(online_net, obs, steps_done)

    def act_greedy(self, online_net, obs):
        """
        :param state:
        :return:
        """
        with torch.no_grad():
            res = online_net(obs).max(1)[1]
            return res.view(-1, 1)

    def act_eps_greedy(self, online_net, state, steps_done):
        """
        Only works for one state (no batches !)
        :param state:
        :return:
        """
        # Linear decay instead of exponentials
        eps_threshold = self.eps_end_value + np.maximum(0, (self.eps_start_value - self.eps_end_value) * (1 - steps_done / self.eps_len_decay))

        sample = random.random()
        if sample > eps_threshold:
            return self.act_greedy(online_net, state)
        else: # Perform a random action
            return torch.tensor([[random.randrange(online_net.output_size)]],
                    dtype=torch.long)

    def update(self, loss):
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

