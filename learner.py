from dqnlearner import DQNLearner
from qlearner import QLearner
from a3clearner import A3CLearner
import gin

@gin.configurable(blacklist=['online_net'])
def learner_builder(online_net, name):
    if name == 'dqn':
        return DQNLearner(online_net)
    elif name == 'qlearner':
        return QLearner()
    elif name == 'a3c':
        return A3CLearner(online_net)
    else:
        return None
