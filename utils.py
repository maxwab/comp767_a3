import torch
import math
import wrappers
import gin
import gym
import random

@gin.configurable
def init_weights(m, type_init='default', normal=False, additional_gain_variance=1):
    r'''Weights initialization used in the MNF exploratin paper'''
    if type_init == 'default':
        pass
    elif type_init == 'stable':
        stable(m, additional_gain_variance)
    elif type_init == 'he':
        he(m, normal, additional_gain_variance)
    elif type_init == 'xavier':
        xavier(m, normal, additional_gain_variance)


def xavier(m, normal=False, agv=1):
    if isinstance(m, torch.nn.Linear):
        if normal:
            torch.nn.init.xavier_normal_(m.weight, gain=math.sqrt(agv)*torch.nn.init.calculate_gain('relu'))
        else: # Uniform
            torch.nn.init.xavier_uniform_(m.weight, gain=math.sqrt(agv)*torch.nn.init.calculate_gain('relu'))
        m.bias.data.zero_()

def stable(m, agv = 1):
    if isinstance(m, torch.nn.Linear):
        n = m.in_features + m.out_features
        stdv = math.sqrt(4*agv / n)
        m.weight.data.normal_(0, stdv)
        if m.bias is not None:
            m.bias.data.zero_()
            #m.bias.data.normal_(0, stdv)

def he(m, normal=False, agv=1):
    if isinstance(m, torch.nn.Linear):
        if normal:
            torch.nn.init.kaiming_normal_(m.weight, a=0)
            m.weight.data *= math.sqrt(agv)
        else: # Uniform
            torch.nn.init.kaiming_uniform_(m.weight, a=0)
            m.weight.data *= math.sqrt(agv)
        m.bias.data.zero_()

@gin.configurable
def create_env(env_name, encoder_name):
    base_env = gym.make(env_name)
    encoder = wrappers.get_encoder(encoder_name)
    return wrappers.CodingWrapper(base_env, encoder)

@gin.configurable
def criterion_builder(name):
    if name == 'huber':
        return torch.nn.SmoothL1Loss
    elif name == 'mse':
        return torch.nn.MSELoss
    else:
        return None

@gin.configurable
def optimizer_builder(name):
    if name == 'adam':
        return torch.optim.Adam
    elif name == 'rms':
        return torch.optim.RMSprop
    else:
        return None

def choose_eps(eps_values):
    return random.choice(eps_values)


