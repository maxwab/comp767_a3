from comet_ml import Experiment
import torch.multiprocessing as mp
import torch
import numpy as np
import gin
import copy
import utils
from logger import tprint
import time
from pathlib import Path
import os
import pickle
import layers

from collections.__init__ import namedtuple
Transition = namedtuple('Transition',
                        ('obs', 'action', 'next_obs', 'reward', 'done'))

class Counter(object):
    r'''
    Code inspired from https://github.com/Feryal/a3c-mujoco/blob/master/utils.py and https://eli.thegreenplace.net/2012/01/04/shared-counter-with-pythons-multiprocessing
    '''

    def __init__(self):
        self.cnt = mp.Value('i', 0)
        self.lock = mp.Lock()

    def increase(self):
        with self.lock:
            self.cnt.value += 1

    def value(self):
        with self.lock:
            return self.cnt.value


@gin.configurable(blacklist=['slave_env', 'id_', 'online_net', 'global_counter'])
def subproc_run(slave_env, id_, online_net, global_counter, max_global_timesteps, _learner, args):
    r'''
    Main function to rollout an environment.
    '''
    torch.set_num_threads(1) # Necessary for quick multiprocessing
    gin.parse_config_file(args.config)

    # Initialisation of the environment
    obs = slave_env.reset()
    proc_counter = 0
    done = False

    # Main loop
    while global_counter.value() <= max_global_timesteps:
        # Synchronize thread specific parameters
        thread_net = copy.deepcopy(online_net)
        thread_net.eval()
        t_start = proc_counter
        l_transitions = []
        for i in range(_learner.period_async_update):
            a = int(_learner.act(thread_net, obs))
            next_obs, r, done, _ = slave_env.step(a)
            l_transitions.append(Transition(obs, a, next_obs, torch.tensor(r).float(), done)) # Create the transition object
            proc_counter += 1
            global_counter.increase()
            obs = next_obs
            if done:
                break

        # Compute the loss and accumulate the gradients
        loss = _learner.compute_loss(thread_net, l_transitions)
        tmp = copy.deepcopy(online_net)
        _learner.update(loss, thread_net, online_net) # Updates the weights of the online (shared) net

        if done:
            obs = slave_env.reset()

    return 0

@gin.configurable(whitelist=['period_testing', 'n_episode_testing'])
def subproc_eval(env, online_net, global_counter, _learner, max_global_timesteps, args, n_episode_testing, period_testing):
    r'''
    Evaluates the current agent for a number of episodes.
    '''

    # Launching comet
    projname = 'a3_a3c_final'
    experiment = Experiment(api_key="7nxUC35g9XtsxFdKcfU2wt0kI", project_name=projname, workspace="maxwab",
            disabled=not args.comet, auto_output_logging=None)
    experiment.log_parameters(vars(args))

    if args.tag is not None:
        experiment.add_tag(args.tag)

    last_test = -period_testing
    l_timesteps = []
    l_times = []
    l_returns = []
    l_entros = []
    t0 = time.time()

    entropy = layers.HLoss()

    # Loop until we have done all episodes, should we test?
    while global_counter.value() <= max_global_timesteps:
        if (global_counter.value() - last_test) >= period_testing:
            last_test = global_counter.value() # Update
            l_timesteps.append(last_test)
            l_times.append(time.time() - t0)

            # We make a copy of the online net to use the same version during all episodes
            evaluation_net = copy.deepcopy(online_net)

            # Testing for several episodes
            evaluation_net.eval()
            total_episode_return = 0
            lent = []
            for _ in range(n_episode_testing):
                obs = env.reset()
                done = False
                while not done:
                    a, allprobs = _learner.act_greedy(evaluation_net, obs)
                    lent.append(entropy(allprobs.view(1,-1)))
                    obs, r, done, _ = env.step(int(a))
                    total_episode_return += r
            avg_episode_return = total_episode_return / n_episode_testing

            l_returns.append(avg_episode_return)

            # Logging to comet-ml
            experiment.log_metric('return', avg_episode_return, step=last_test)
            # Entropy
            entro_val = torch.stack(lent).mean()
            l_entros.append(entro_val)
            experiment.log_metric('entropy', entro_val, step=last_test)

            # Display if verbose
            if args.verbose:
                tprint('Return after {} timesteps: {}'.format(last_test, avg_episode_return))

        time.sleep(1) # We don't want to test too frequently, so we wait at least 1ms.

    # After we are done, we save the returns
    # Creating the folder
    savepath = Path(args.save_folder)
    if not Path.exists(savepath):
        os.makedirs(savepath)

    testing_results = np.stack([np.array(l_timesteps), np.array(l_times), np.array(l_returns), np.array(l_entros)], 1)
    # Saving the results
    try:
        filename = 'results_seed:{}'.format(args.seed)
        if args.id is not None:
            filename += '_id:{}'.format(args.id)
        filename += '.npy'
        with open(savepath / filename, 'wb') as fd:
            np.save(fd, testing_results)
    except:
        print('Failed to save file {} at location {}'.format(filename, savepath))

    return 0

