from qlearner import QLearner
import torch
import copy
from collections.__init__ import namedtuple
Transition = namedtuple('Transition',('obs', 'action', 'next_obs', 'reward', 'done'))
q = QLearner(1, 1, [1], 2, False, 2)
obs = torch.zeros((1, 100))
obs[0,4] = 1
next_obs = torch.zeros((1, 100))
next_obs[0,5] = 1
t = Transition(obs, 0, next_obs, 1., False)
w = torch.randn((1, 300))
l1 = q.compute_loss(t, w, 0.1)
next_next_obs = torch.zeros((1, 100))
next_next_obs[0,12] = 1
t2 = Transition(next_obs, 0, next_next_obs, .5, False)
l2 = q.compute_loss(t2, w, 0.1)
wtemp = copy.deepcopy(w)
q.update(l1+l2, w)
print(w-wtemp)
