import torch.nn as nn
import torch.nn.functional as F
import layers
import gin
import utils

@gin.configurable(blacklist=['input_size', 'output_size'])
class MLP2(nn.Module):
    r'''
    Simple 2 hidden layers MLP, with possible dropout
    '''
    def __init__(self, input_size, output_size, n_neurons, dropout_rate):
        super().__init__()
        self.input_size = input_size
        self.output_size = output_size

        # Architecture construction
        self.fc1 = nn.Linear(input_size, n_neurons)
        self.fc1_dropout = nn.Dropout(p=dropout_rate)
        self.fc2 = nn.Linear(n_neurons, n_neurons)
        self.fc2_dropout = nn.Dropout(p=dropout_rate)
        self.fco = nn.Linear(n_neurons, output_size)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc1_dropout(x)
        x = F.relu(self.fc2(x))
        x = self.fc2_dropout(x)
        return self.fco(x)


@gin.configurable(blacklist=['env'])
def mlp2_builder(env, input_dim):
    def _builder():
        net = MLP2(input_dim, env.action_space.n)
        net.apply(utils.init_weights)
        return net
    return _builder

@gin.configurable()
class LinearQNetwork(nn.Module):
    def __init__(self, input_size, n_actions):
        super().__init__()
        self.input_size = input_size
        self.output_size = n_actions

        # Architecture construction
        self.fc1 = nn.Linear(input_size, n_actions)

    def forward(self, x):
        x = self.fc1(x)
        return x

@gin.configurable(blacklist=['env'])
def linear_qnetwork_builder(env, n_features):
    def _builder():
        net = LinearQNetwork(n_features, env.action_space.n)
        net.apply(utils.init_weights)
        return net
    return _builder


@gin.configurable
class LinearAdvantageActorCritic(nn.Module):
    r'''
    Code from pytorch tutorial on DQN. Note: this is not the Nature CNN base that everyone uses.
    '''

    def __init__(self, n_features, n_actions):
        super().__init__()
        self.fc_actor = nn.Linear(n_features, n_actions, bias=False)
        self.sm = nn.Softmax(1) # Policy head
        self.lsm = nn.LogSoftmax(1) # Policy head, but for log
        self.fc_critic = nn.Linear(n_features, 1, bias=False)

    def forward(self, phi):
        return self.forward_actor(phi), self.forward_critic(phi)

    def forward_actor(self, phi):
        x = self.fc_actor(phi)
        pi = self.sm(x)
        return pi

    def logforward_actor(self, phi):
        x = self.fc_actor(phi)
        logpi = self.lsm(x)
        return logpi

    def forward_critic(self, phi):
        x = self.fc_critic(phi)
        return x

@gin.configurable(blacklist=['env'])
def linear_a3c_builder(env, n_features):
    def _builder():
        net = LinearAdvantageActorCritic(n_features, env.action_space.n)
        net.apply(utils.init_weights)
        return net
    return _builder
