import model
import torch
import torch.optim as optim
import torch.nn as nn
import numpy as np
import random
import layers
import gin
import copy
import utils
import wrappers

@gin.configurable
class QLearner(object):
    name = 'qlearner'
    def __init__(self,
            discount_factor, eps_start_value, possible_eps_end_values, eps_len_decay,
            double_q, period_async_update, alpha, device=torch.device('cpu')):

        self.double_q = double_q
        self.discount_factor = discount_factor
        # Preparing the loss function
        self.eps_start_value = eps_start_value
        self.eps_end_value = utils.choose_eps(possible_eps_end_values)
        self.eps_len_decay = eps_len_decay
        self.period_async_update = period_async_update
        self.action_space_shape = 3 # TODO : change this, hardcoded
        self.alpha = alpha

        # Giving information on the type of learner
        print('Learner: eps_end_value={}'.format(self.eps_end_value))

    def compute_loss(self, transition, w):
        r'''
        Returns the loss vector associated with a transition
        \alpha (Rt+1 + max_aQ(St+1, a) - Q(St, At))
        Which is the term that we add to the weight vector
        Reminder: this works with linear FA.
        '''

        # First, encode the state and action into a representation \phi
        phi = wrappers.phi_encode(transition.obs, transition.action)
        phi_nexts = [wrappers.phi_encode(transition.next_obs, a) for a in range(self.action_space_shape)]

        # Then compute the Q target
        if not self.double_q:
            if transition.done: # This condition avoids a costly forward pass if the state is terminal
                qlearning_target = transition.reward
            else:

                next_max_state_action_values = torch.stack([torch.masked_select(w, phi_next.byte()).sum() for phi_next in phi_nexts]).max(0)[0]
                qlearning_target = transition.reward + self.discount_factor * next_max_state_action_values # Scalar since we are evaluating on only one sample
        else:
            # TODO : to implement
            pass

        state_action_values = torch.masked_select(w, phi.byte()).sum()  # Compute Q(s_t, a), only one element per batch

        # Actually compute the loss
        loss = self.alpha * (qlearning_target - state_action_values)*phi
        return loss

    def act(self, w, obs, steps_done):
        return self.act_eps_greedy(w, obs, steps_done)

    def act_greedy(self, w, obs):
        """
        :param obs:
        :return:
        """
        phi_nexts = [wrappers.phi_encode(obs, a) for a in range(self.action_space_shape)]
        action = torch.stack([torch.masked_select(w, phi_next.byte()).sum() for phi_next in phi_nexts]).max(0)[1]
        return action.view(1,-1)

    def act_eps_greedy(self, w, obs, steps_done):
        """
        Only works for one state (no batches !)
        :param state:
        :return:
        """
        # Linear decay instead of exponentials
        eps_threshold = self.eps_end_value + np.maximum(0, (self.eps_start_value - self.eps_end_value) * (1 - steps_done / self.eps_len_decay))

        sample = random.random()
        if sample > eps_threshold:
            return self.act_greedy(w, obs)
        else: # Perform a random action
            return torch.tensor([[random.randrange(self.action_space_shape)]], dtype=torch.long)

    def update(self, loss, weights):
        weights.add_(loss)
