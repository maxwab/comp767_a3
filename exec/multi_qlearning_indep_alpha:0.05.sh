cd ..
python main_qlearning.py --config configs/qlearning/multi_qlearning_indep_alpha:.05.gin --seed 0 --verbose --comet --save_folder log/qlearning_multi_indep_alpha:.05 --id 0 --tag final &
python main_qlearning.py --config configs/qlearning/multi_qlearning_indep_alpha:.05.gin --seed 1 --verbose --comet --save_folder log/qlearning_multi_indep_alpha:.05 --id 1 --tag final &
python main_qlearning.py --config configs/qlearning/multi_qlearning_indep_alpha:.05.gin --seed 2 --verbose --comet --save_folder log/qlearning_multi_indep_alpha:.05 --id 2 --tag final &
python main_qlearning.py --config configs/qlearning/multi_qlearning_indep_alpha:.05.gin --seed 3 --verbose --comet --save_folder log/qlearning_multi_indep_alpha:.05 --id 3 --tag final &
python main_qlearning.py --config configs/qlearning/multi_qlearning_indep_alpha:.05.gin --seed 4 --verbose --comet --save_folder log/qlearning_multi_indep_alpha:.05 --id 4 --tag final &
wait
