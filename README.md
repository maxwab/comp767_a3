# COMP767 - Assignment #3

This repository contains the code for the assignment #3 of the COMP-767 class.

# Files
- {qlearner,dqnlearner,a3clearner}.py : Tabular Q-Learning, DQN and A3C algorithms
- learner.py : builder used by gin-config to parameterize the characterization of the algorithm to use.
- layers.py : contains some used losses and particular layers.
- model.py : contains the models of the neural nets used.
- logger.py : contains functions used for logging.
- {main_qlearning,main,main_a3c}.py : contains the main file that creates the processes and saved the model at the end respectively for Q-Learning, DQN and A3C.
- {mpenvsqlearning,mpenvs,mpenvsa3c}.py : contains the code that each process runs to interact with the environment, as well as the evaluator process that evaluates the performance of the trained net, respectively for Q-Learning, DQN and A3C.
- tiles.py : used for tile coding.
- utils.py : contains weight initialization functions as well as builders used by gin-config.
- wrappers.py : contains classes that wrap around a gym environment and encode the states.

# Used repositories
I was inspired by the following repositories for certain implementation details, such as the use of a Lock for the counter:
- https://github.com/Feryal/a3c-mujoco
- https://github.com/wuhuikai/a3c-deepmind-pytorch

# How to run experiments
The configuration files used to run the experiments are all available in ``configs/``.
An experiment can be run using
``python main(_a3c/_qlearning).py --config configs/XXX.gin``

The figures can be generated once the experiments have been run using the files in ``visualisation``.
