# File to be edited each time to generate the conrfigurations of the experiment
from jinja2 import Template
import os
from pathlib import Path

experiment_name = 'dqn_more_processes'
p = Path(experiment_name)
p_exec = Path('../exec')

with open('dqn_template.jinja', 'r') as fd:
    t = Template(fd.read())

# Create the directory, throw an error if it already exists
try:
    os.makedirs(p)
except:
    raise ValueError('Error in the directory creation.')

# Creation of the files
n_processes = [1, 2, 4, 6, 8, 16]
l_filenames = []
for n in n_processes:
    filename = 'n={}'.format(n)
    l_filenames.append(filename)
    t.stream(n_process=n).dump(str(p / '{}.gin'.format(filename)))

# Creation of the exec fil
with open('exec_template.jinja', 'r') as fd:
    exec_t = Template(fd.read(), trim_blocks=True, lstrip_blocks=True)

for filename in l_filenames:
    exec_t.stream(experiment_name=experiment_name, filenames = l_filenames, n_seeds=5).dump(str(p_exec / '{}.sh'.format(experiment_name)))
