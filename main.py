from comet_ml import Experiment
import torch.multiprocessing as mp
import torch, numpy as np, random
import mpenvs
import copy
import utils
import comet_ml
import learner
import gin
import argparse as ap
import model
from logger import tprint
import os
from pathlib import Path

# Parts of the code copied from
# https://pytorch.org/docs/stable/notes/multiprocessing.html
@gin.configurable
def run(args, n_processes, max_global_timesteps, n_episode_testing, period_testing):

    # Fixing seeds for reproducibility:
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)
    random.seed(args.seed)
    mp.set_start_method('spawn')

    # Creating environments
    envs = [utils.create_env() for _ in range(n_processes)]
    evaluation_env = utils.create_env()

    # Creating models
    online_net = model.linear_qnetwork_builder(envs[0])()
    online_net.share_memory()
    target_net = copy.deepcopy(online_net)
    target_net.eval()

    global_counter = mpenvs.Counter()

    # Creation and launch of the different environments
    processes = []

    # First we launch the evaluation process, so that it begins at the first timestep
    _learner = learner.learner_builder(online_net)
    p_eval = mp.Process(target=mpenvs.subproc_eval, args=(evaluation_env, online_net, global_counter, _learner, max_global_timesteps, args, n_episode_testing, period_testing))
    p_eval.start()

    for i, env in enumerate(envs):
        _learner = learner.learner_builder(online_net)
        p = mp.Process(target=mpenvs.subproc_run, args=(env, i, online_net, target_net, global_counter, max_global_timesteps, _learner, args))
        p.start()
        tprint('Process {} created.'.format(p.name))
        processes.append(p)
    processes.append(p_eval) # We add the evaluation processes at the end of the loop

    for p in processes:
        p.join()

    # Creating the folder
    savepath = Path(args.save_folder)
    if not Path.exists(savepath):
        os.makedirs(savepath)

    # Saving the model only when we are sure that it's not going to be modified anymore
    modelname = 'model_seed:{}'.format(args.seed)
    if args.id is not None:
        modelname += '_id:{}'.format(args.id)
    modelname += '.pt'
    with open(savepath / modelname, 'wb') as fd:
        torch.save(online_net.state_dict(), fd)

    tprint('Done')

if __name__ == '__main__':
    parser = ap.ArgumentParser()
    # RL flags
    parser.add_argument('--config', type=str, required=True, help='Path to the gin configuration fiile')
    parser.add_argument('--seed', type=int, default=0)
    # General flags
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--save_folder', type=str, default='log/mc')
    parser.add_argument('--id', type=int)
    parser.add_argument('--comet', action='store_true')

    args = parser.parse_args()
    gin.parse_config_file(args.config)

    run(args)
